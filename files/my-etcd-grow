#!/bin/bash
set -Eeuo pipefail
. ${MY_ENVFILE:-/etc/default/mygw}
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}

: ${MY_ETCD_LOCAL_HOST:=$(hostname)}							## local hostname
: ${MY_ETCD_HOSTNAME:="etcd.$(hostname -d)"}					## hostname resolving to all available etcd nodes
: ${MY_KUBERNETES_API_HOSTNAME:="kubernetes.$(hostname -d)"}	## hostname of the k8s api is listening on, prob another multi-IP DNS name
: ${MY_TMPDIR:="$(mktemp -td my-init-XXXXX)"}

#for ssl certs
: ${MY_SSL_COUNTRY:=XX}
: ${MY_SSL_LOCATION:=woIwill}
: ${MY_SSL_STATE:=dohoam}


if ! testipv6 "${MY_K8S_SERVICE_IP_RANGE:-}"; then
	echo "please set MY_K8S_SERVICE_IP_RANGE (service subnet, /112 or smaller) var accordingly..."
	exit 1
fi

cd ${MY_TMPDIR}

for nstest in ${MY_ETCD_HOSTNAME} ${MY_KUBERNETES_API_HOSTNAME}; do
	if [ -z "$(dig +short aaaa ${nstest})" ]; then
		cat <<-EOF
			##############################################
			###
			### ${nstest} does not resolve, I need the master to already be working
			### ${nstest} should eventually resolve to all etcd/api nodes.
			### make sure you extend this as you add more nodes of this type
			### but for now just make sure it works at all....
			###
			##############################################
		EOF
		exit 1
	fi
done


echo "## getting deps..."
sudo apt-get update
sudo apt-get install etcd


## gen etcd cert
cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${MY_SSL_COUNTRY}",
      "L": "${MY_SSL_LOCATION}",
      "ST": "${MY_SSL_STATE}",
      "O": "Kubernetes",
      "OU": "Kubernetes API"
    }
  ]
}
EOF

KUBERNETES_HOSTNAMES="${MY_KUBERNETES_API_HOSTNAME},${MY_ETCD_HOSTNAME},$(hostname),kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local"
KUBERNETES_IPS="$(ip -o addr show lo | awk '{ print $4 }' | sed 's|/.*||' | xargs echo | sed 's/ /,/g')"

cat <<EOF
#########################################################################################################
#########################################################################################################
###
###  WARNING:
###
###  make sure you start ths SSL signer api on the controller node ${MY_KUBERNETES_API_HOSTNAME}
###  you can do that by running following command on ${MY_KUBERNETES_API_HOSTNAME} as root:
###  'my-ssl-signer'
###  this will allow me to get my certs signed, which I need in order to join the cluster...
###  be sure to stop the signer ASAP to prevent unauthorized signatures!
###
###  hit return when ready...
###
#########################################################################################################
#########################################################################################################
EOF
read IDONTCARE

# get ca cert
curl -sd '{"label": "primary"}' --fail http://${MY_KUBERNETES_API_HOSTNAME}:1234/api/v1/cfssl/info | jq -r '.result | .certificate' >ca.pem
# get signed etcd cert
cfssl gencert -remote ${MY_KUBERNETES_API_HOSTNAME}:1234 -profile=kubernetes -hostname=${KUBERNETES_HOSTNAMES},${KUBERNETES_IPS},${MY_K8S_SERVICE_IP_RANGE%/*}1 kubernetes-csr.json | cfssljson -bare kubernetes

cat <<EOF
#########################################################################################################
#########################################################################################################
###
### WARNING: I'm done signing, stop the signer on ${MY_KUBERNETES_API_HOSTNAME} asap by hitting CTRL+C.
###
#########################################################################################################
#########################################################################################################
EOF


## setup etcd cluster
sudo mkdir -p /etc/etcd /var/lib/etcd
sudo mv ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/
sudo chown etcd /etc/etcd/kubernetes-key.pem
sudo chown etcd /var/lib/etcd
sudo chmod 700 /var/lib/etcd


MY_ETCD_EXISTING_PEERS=$(sudo ETCDCTL_API=3 etcdctl member list --endpoints=https://${MY_ETCD_HOSTNAME}:2379 --cacert=/etc/etcd/ca.pem --cert=/etc/etcd/kubernetes.pem --key=/etc/etcd/kubernetes-key.pem --write-out=json | jq -r '.members[] | select(.name!=null) | "\(.name)=\(.peerURLs[])"')

for peer in ${MY_ETCD_EXISTING_PEERS}; do
	l=${l:-},${peer}
done
ETCD_INITIAL_CLUSTER="${l#,},${MY_ETCD_LOCAL_HOST}=https://${MY_ETCD_LOCAL_HOST}:2380"


ETCDCTL_API=3 etcdctl --endpoints=https://${MY_ETCD_HOSTNAME}:2379 --cacert=/etc/etcd/ca.pem --cert=/etc/etcd/kubernetes.pem --key=/etc/etcd/kubernetes-key.pem member add ${MY_ETCD_LOCAL_HOST} --peer-urls="https://${MY_ETCD_LOCAL_HOST}:2380"


sudo tee /etc/default/etcd >/dev/null <<EOF
ETCD_NAME="${MY_ETCD_LOCAL_HOST}"
ETCD_CERT_FILE="/etc/etcd/kubernetes.pem"
ETCD_KEY_FILE="/etc/etcd/kubernetes-key.pem"
ETCD_PEER_CERT_FILE="/etc/etcd/kubernetes.pem"
ETCD_PEER_KEY_FILE="/etc/etcd/kubernetes-key.pem"
ETCD_TRUSTED_CA_FILE="/etc/etcd/ca.pem"
ETCD_PEER_TRUSTED_CA_FILE="/etc/etcd/ca.pem"
ETCD_PEER_CLIENT_CERT_AUTH="true"
ETCD_CLIENT_CERT_AUTH="true"
ETCD_LISTEN_PEER_URLS="https://[::]:2380"
ETCD_LISTEN_CLIENT_URLS="https://[::]:2379"
ETCD_ADVERTISE_CLIENT_URLS="https://${MY_ETCD_HOSTNAME}:2379"
ETCD_INITIAL_CLUSTER_STATE="existing"
ETCD_INITIAL_CLUSTER="${ETCD_INITIAL_CLUSTER}"
ETCD_DATA_DIR="/var/lib/etcd"
EOF


sudo systemctl enable etcd
sudo systemctl restart etcd

sleep 2

cd -
rm -rf ${MY_TMPDIR}

sudo ETCDCTL_API=3 etcdctl member list --endpoints=https://${MY_ETCD_HOSTNAME}:2379 --cacert=/etc/etcd/ca.pem --cert=/etc/etcd/kubernetes.pem --key=/etc/etcd/kubernetes-key.pem
