#!/bin/bash
set -Eeuo pipefail
. ${MY_ENVFILE:-/etc/default/mygw}
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}

: ${MY_KUBERNETES_API_HOSTNAME:="kubernetes.$(hostname -d)"}			## hostname of the k8s api is listening on, prob another multi-IP DNS name
: ${MY_KUBERNETES_NODENAME:="$(hostname)"}
: ${MY_KUBERNETES_CLUSTER_NAME:="meiniger"}
: ${MY_KUBERNETES_POD_SUBNET:="$(jq -r '.ipam.subnet' /etc/cni/net.d/10-bridge.conf)"}
: ${MY_KUBERNETES_DOMAIN:="k8s.$(hostname -d)"}
: ${MY_KUBERNETES_DNS_MASTER:="$(dig +norecurse +noall +authority ns ${MY_KUBERNETES_DOMAIN} | awk '{ print $5 }')"}
: ${MY_KUBERNETES_DNS_IP:="$(dig +short aaaa ${MY_KUBERNETES_DNS_MASTER})"}
: ${MY_TMPDIR:="$(mktemp -td my-init-XXXXX)"}

#for ssl certs
: ${MY_SSL_COUNTRY:=XX}
: ${MY_SSL_LOCATION:=woIwill}
: ${MY_SSL_STATE:=dohoam}

mount | grep -q "^cgroup " || panic "cgroup v2 enabled on this system but unfortunately not yet supported ;/ for now make sure to set systemd.unified_cgroup_hierarchy=0 in grub/kernel cmdline"

testipv6 ${MY_KUBERNETES_DNS_IP} || panic "unable to detect MY_KUBERNETES_DNS_IP: ${MY_KUBERNETES_DNS_IP} and not passed as env var either, I need to know this..."

cd ${MY_TMPDIR}

## gen ssl certs config
# kubelet
cat > ${MY_KUBERNETES_NODENAME}-csr.json <<EOF
{
  "CN": "system:node:${MY_KUBERNETES_NODENAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${MY_SSL_COUNTRY}",
      "L": "${MY_SSL_LOCATION}",
      "ST": "${MY_SSL_STATE}",
      "O": "system:nodes",
      "OU": "Kublets"
    }
  ]
}
EOF


## gen kube proxy cert
cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${MY_SSL_COUNTRY}",
      "L": "${MY_SSL_LOCATION}",
      "ST": "${MY_SSL_STATE}",
      "O": "system:node-proxier",
      "OU": "kube-proxier"
    }
  ]
}
EOF


cat <<EOF
#########################################################################################################
#########################################################################################################
###
###  WARNING:
###
###  make sure you start ths SSL signer api on the controller node ${MY_KUBERNETES_API_HOSTNAME}
###  you can do that by running following command on ${MY_KUBERNETES_API_HOSTNAME} as root:
###  'my-ssl-signer'
###  this will allow me to get my certs signed, which I need in order to join the cluster...
###  be sure to stop the signer ASAP to prevent unauthorized signatures!
###
###  hit return when ready...
###
#########################################################################################################
#########################################################################################################
EOF
read IDONTCARE

# get ca cert
curl -sd '{"label": "primary"}' --fail http://${MY_KUBERNETES_API_HOSTNAME}:1234/api/v1/cfssl/info | jq -r '.result | .certificate' >ca.pem
# get signed kubelet cert
cfssl gencert -remote ${MY_KUBERNETES_API_HOSTNAME}:1234 -hostname=${MY_KUBERNETES_NODENAME} -profile=kubernetes ${MY_KUBERNETES_NODENAME}-csr.json | cfssljson -bare ${MY_KUBERNETES_NODENAME}
# get signed kube-proxy cert
cfssl gencert -remote ${MY_KUBERNETES_API_HOSTNAME}:1234 -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy

cat <<EOF
#########################################################################################################
#########################################################################################################
###
### WARNING: I'm done signing, stop the signer on ${MY_KUBERNETES_API_HOSTNAME} asap.
###
#########################################################################################################
#########################################################################################################
EOF

## kubeconfig for kubelet
kubectl config set-cluster ${MY_KUBERNETES_CLUSTER_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${MY_KUBERNETES_API_HOSTNAME}:6443 \
    --kubeconfig=${MY_KUBERNETES_NODENAME}.kubeconfig

kubectl config set-credentials system:node:${MY_KUBERNETES_NODENAME} \
    --client-certificate=${MY_KUBERNETES_NODENAME}.pem \
    --client-key=${MY_KUBERNETES_NODENAME}-key.pem \
    --embed-certs=true \
    --kubeconfig=${MY_KUBERNETES_NODENAME}.kubeconfig

kubectl config set-context default \
    --cluster=${MY_KUBERNETES_CLUSTER_NAME} \
    --user=system:node:${MY_KUBERNETES_NODENAME} \
    --kubeconfig=${MY_KUBERNETES_NODENAME}.kubeconfig

kubectl config use-context default --kubeconfig=${MY_KUBERNETES_NODENAME}.kubeconfig



## kubeconfig for kube proxy
kubectl config set-cluster ${MY_KUBERNETES_CLUSTER_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${MY_KUBERNETES_API_HOSTNAME}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-credentials system:kube-proxy \
    --client-certificate=kube-proxy.pem \
    --client-key=kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config set-context default \
    --cluster=${MY_KUBERNETES_CLUSTER_NAME} \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig






sudo mkdir -p /var/lib/kubelet /var/lib/kube-proxy /var/lib/kubernetes /var/run/kubernetes



## kubelet config
sudo mv ${MY_KUBERNETES_NODENAME}-key.pem ${MY_KUBERNETES_NODENAME}.pem /var/lib/kubelet/
sudo mv ${MY_KUBERNETES_NODENAME}.kubeconfig /var/lib/kubelet/kubeconfig
sudo mv ca.pem /var/lib/kubernetes/


sudo tee /var/lib/kubelet/kubelet-config.yaml >/dev/null <<EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "${MY_KUBERNETES_DOMAIN}"
clusterDNS:
  - "${MY_KUBERNETES_DNS_IP}"
podCIDR: "${MY_KUBERNETES_POD_SUBNET}"
resolvConf: "/run/systemd/resolve/resolv.conf"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/${MY_KUBERNETES_NODENAME}.pem"
tlsPrivateKeyFile: "/var/lib/kubelet/${MY_KUBERNETES_NODENAME}-key.pem"
EOF




sudo tee /etc/systemd/system/kubelet.service >/dev/null <<EOF
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/kubernetes/kubernetes
After=containerd.service
Requires=containerd.service

[Service]
ExecStart=/usr/bin/kubelet \\
  --feature-gates="IPv6DualStack=true" \\
  --config=/var/lib/kubelet/kubelet-config.yaml \\
  --container-runtime=remote \\
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
  --image-pull-progress-deadline=2m \\
  --kubeconfig=/var/lib/kubelet/kubeconfig \\
  --network-plugin=cni \\
  --register-node=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF





## kube-proxy config...

sudo mv kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig
sudo tee /var/lib/kube-proxy/kube-proxy-config.yaml >/dev/null <<EOF
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
bindAddress: "::"
clientConnection:
  kubeconfig: "/var/lib/kube-proxy/kubeconfig"
mode: "iptables"
EOF


sudo tee /etc/systemd/system/kube-proxy.service >/dev/null <<EOF
[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/bin/kube-proxy \\
  --feature-gates="IPv6DualStack=true" \\
  --config=/var/lib/kube-proxy/kube-proxy-config.yaml
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF




sudo systemctl daemon-reload
sudo systemctl enable containerd kubelet kube-proxy systemd-resolved
sudo systemctl start containerd kubelet kube-proxy systemd-resolved

cd -
rm -rf ${MY_TMPDIR}

## re-do radvd config and start advertising the serivce subnet
my-announce-prefix
